========================
Packaging packagegrinder
========================

Using an installed package grinder to build an RPM of a new version
doesn't work, since the package can't be running from /opt while the
grinder is building it there.

This needs to be run on a machine where the packaging user has write
access to /opt and has an appropriate ~/.buildout/default.cfg.  This can
be achieved by using the jenkins user on grinder.dyn.zope.net.

The packagegrinder RPM can be built using a git clone of the software in
some other location.  Check out the tag you want to package (because
that's supposed to be a working version) and build normally::

    $ git clone 'git@bitbucket.org:zc/packagegrinder.git' working
    $ cd working/
    $ git checkout 1.1.0
    $ /opt/bootstrap33/bin/buildout bootstrap
    $ bin/buildout

This built copy can now be used to package the same version of the
software::

    $ sudo -u jenkins -H \
          bin/build packagegrinder 1.3.0 \
          --destination 'app.rpm.nova.aws.zope.net:/build/zc/{distrover}/'
