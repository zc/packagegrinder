==============
packagegrinder
==============

This tool ("the grinder") builds installable packages for ZC managed
machines.  For the moment, this means RPMs.  Software is built starting
from sources in version control.

The release tag must already exist in version control; it will not be
created by the grinder.


Package requirements
--------------------

The following files may be provided by the package to control how
package-grinder constructs RPMs:

``packaging-build``
    A script run in the exported source tree to build the software in
    place prior to RPM construction.  If omitted, no build process is
    run; the content of the checkout needs to be installation-ready.

    If building Python software using ``zc.buildout``, this is
    responsible for bootstrapping, building, and compiling ``.pyc``
    files that should be installed.  The build should *not* suppress
    user configuration, but there are constraints on the user
    configuration that can be used.

    This script can assume that build requirements are available, and
    should not invoke **rpm** or **yum**.

``requires.json``
    A structure describing the run-time and build-time requirements for
    the software.  Optional.

``rpm.spec.in``
    A fragment of an RPM spec file; this is included as part of the spec
    file used to generate the software.  This can usually be omitted.

.. We should document the requires.json structure somewhere that can be
   shared with zkdeployment.


``zc.buildout`` environments
----------------------------

The ``packaging-build`` script should not suppress user default
configuration because we want to use caching controlled through the
``~/.buildout/default.cfg`` file in the Jenkins environment.

Specifically, because we want the RPMs to contain complete copies of
their eggs, no egg cache can be specified using ``eggs-directory``.


Running the grinder
-------------------

The grinder can be run manually or under Jenkins control.  There are two
required parameters given on the command line, and a number of options
to affect the generated RPM.

The required options are the package name and version::

    $ /opt/packagegrinder/bin/build openidprovider 1.10.2

The grinder computes the version control location for the software,
the installation location, and exports the software into that location
on the local machine.  The software is then built in place, and the RPM
constructed from that.

There is a utility option, ``--show``, that causes the grinder to
display the computed information about the RPM without performing any of
the build or packaging operations; let's use that to see what's
generated for the command line above::

    $ /opt/packagegrinder/bin/build openidprovider 1.10.2 --show
    Name: openidprovider
    Version: 1.10.2
    Release: 1
    Location: /opt/openidprovider
    VC URL: svn+ssh://svn.zope.com/repos/main/zc.openidprovider/tags/openidprovider-1.10.2-1

Nice, but we want to install multiple versions of the software in
parallel, so let's add the ``--parallel`` option::

    $ /opt/packagegrinder/bin/build openidprovider 1.10.2 --parallel --show
    Name: openidprovider-1.10.2
    Version: 1.10.2
    Release: 1
    Location: /opt/openidprovider-1.10.2
    VC URL: svn+ssh://svn.zope.com/repos/main/zc.openidprovider/tags/openidprovider-1.10.2-1

Note that the name of the RPM and the installation location now include
the specific version of the software.

We can also build software hosted in git::

    $ /opt/packagegrinder/bin/build packagegrinder 1.0.0 \
          --vc-url 'ssh://git@bitbucket.org/zc/packagegrinder.git' --show
    Name: packagegrinder
    Version: 1.0.0
    Release: 1
    Location: /opt/packagegrinder
    VC URL: ssh://git@bitbucket.org/zc/packagegrinder.git

When using **git** instead of **svn**, the tag name used to build from
must match the version exactly.  With **svn**, the version need not
match the tag if ``--vc-url`` is used.


Release history
---------------


1.3.2 (2015-01-15)
~~~~~~~~~~~~~~~~~~

- Fix preun script to be more careful about linking software.tar.


1.3.1 (2014-12-24)
~~~~~~~~~~~~~~~~~~

- Deal with **rpm -q** reporting a package more than once due to
  platform multi-arch behavior.


1.3.0 (2014-12-22)
~~~~~~~~~~~~~~~~~~

- Add script to install dependencies as specified in a ``requires.json``
  file, primarily for use in jenkins jobs.
- Adjust package installation to avoid updating installed packages that
  satisfy requirements.

  
1.2.6 (2013-09-24)
~~~~~~~~~~~~~~~~~~

- Update packaging-build to avoid getting out of date with respect to
  the versions in the buildout configuration.


1.2.5 (2013-09-24)
~~~~~~~~~~~~~~~~~~

- Fix syntactical error in shell script in the generated spec file.
- Update to the latest in buildout and setuptools.


1.2.4 (2013-08-16)
~~~~~~~~~~~~~~~~~~

- Report failure when specified software doesn't exist in version
  control (tag doesn't exist, for example).


1.2.3 (2013-08-05)
~~~~~~~~~~~~~~~~~~

- Fix bug that caused deploying an update to a non-parallel installation
  to create a nearly empty installation directory without generating an
  error.


1.2.2 (2013-07-10)
~~~~~~~~~~~~~~~~~~

- Force world-read on software to work around issue with eggs built from
  tar-based sdists with restricted permissions.  Avoids inflicting the
  pain on all ``packaging-build`` scripts.
- Fix the scriptlets in the RPM spec to deal correctly with all flavors
  of installation, including updating from pre-grinder versions.


1.2.1 (2013-07-08)
~~~~~~~~~~~~~~~~~~

- Fix the scriptlets in the RPM spec to deal correctly with non-parallel
  installations.
- Fix ownership of files unpacked from the RPM-contained tarball.


1.2.0 (2013-05-24)
~~~~~~~~~~~~~~~~~~

- Read ``requires.json`` from the source export to obtain the run-time
  and build-time requirements.

- Use **yum** to install the build-time requirements.


1.1.0 (2013-05-22)
~~~~~~~~~~~~~~~~~~

- New ``--destination`` option specifies where to copy the generated
  RPM (defaults to current directory).

- Re-implemented in Python.


1.0.1 (2013-05-03)
~~~~~~~~~~~~~~~~~~

Built as a ``noarch`` RPM, since the grinder is not
architecture-specific.

Added missing dependencies for the RPM.


1.0.0 (2013-05-03)
~~~~~~~~~~~~~~~~~~

Initial release.
