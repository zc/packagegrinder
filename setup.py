import setuptools

setuptools.setup(
    name="zc.packagegrinder",
    version="0",
    package_dir={"": "src"},
    packages=setuptools.find_packages("src"),
    namespace_packages=["zc"],
    install_requires=[
        "setuptools",
        "zope.testing",
        ],
    entry_points={
        "console_scripts": [
            "build = zc.packagegrinder:main",
            "install-requirements = zc.packagegrinder:install_requirements",
            ],
        "version_control_plugins": [
            "git = zc.packagegrinder.git:Git",
            "subversion = zc.packagegrinder.subversion:Subversion",
            ],
        },
    zip_safe=False,
    )
