"""\
Package-building tool.

"""

import argparse
import collections
import contextlib
import errno
import glob
import json
import os
import pkg_resources
import shutil
import subprocess
import sys
import tempfile


PREFIX = "/opt"
TARNAME = "software.tar"


def vc_plugins():
    plugins = list(
        pkg_resources.iter_entry_points("version_control_plugins"))
    plugins.sort(key=lambda ep: ep.name)
    return plugins


parser = argparse.ArgumentParser(
    description="Generate installation packages from source repositories.")

parser.add_argument(
    "-p", "--parallel", action="store_true", default=False,
    help="Support parallel installations of the package.")

parser.add_argument(
    "--project", action="store",
    help="Name of the project in the version control repository.")

parser.add_argument(
    "--show", action="store_true", default=False,
    help=("Display final settings used to build the installation package,"
          " instead of building it."))

parser.add_argument(
    "--vc-url", action="store", dest="vc_url",
    help="Specify the version control URL to use.")

parser.add_argument(
    "--destination", action="store", default=os.getcwd(),
    metavar="HOST:PATH",
    help=("Write the finished RPM to the specified location."
          " Include '{distrover}' to indicate the major version of"
          " the O/S platform.  HOST: is optional, and may be omitted"
          " for paths on the build host."
          ))

for ep in vc_plugins():
    if ep.name == "git":
        help = "Use git for version control (the default)."
    else:
        help = "Use {} for version control.".format(ep.name)
    parser.add_argument(
        "--" + ep.name, action="store_const", dest="vc_plugin", const=ep.name,
        default="git",
        help=help)

parser.add_argument(
    "name", action="store",
    help="Name of the installation package (i.e., 'z4m').")

parser.add_argument(
    "version", action="store",
    help="Version of the package being built.")


def parse_args(args):
    options = parser.parse_args(args)
    if not options.project:
        options.project = options.name
    if "{" in options.destination:
        # Get the CentOS version we're building for:
        p = subprocess.Popen(
            ["rpm", "-q", "--whatprovides", "redhat-release"],
            stdout=subprocess.PIPE)
        distrover = str(p.communicate()[0], "utf-8").rsplit("-", 2)[1]
        options.destination = options.destination.format(distrover=distrover)
    return options


def get_version_control_plugin(options):
    eps = list(ep for ep in vc_plugins() if ep.name == options.vc_plugin)
    # There can be only one.
    ep, = eps
    return ep.load()(options)


@contextlib.contextmanager
def workspace():
    with tempfile.TemporaryDirectory(prefix="grinder-") as scratch:
        with open(os.path.join(scratch, "macros"), "w") as f:
            print("%_builddir", scratch, file=f)
            print("%_rpmdir  ", scratch, file=f)
            print("%_topdir  ", scratch, file=f)
        with open(os.path.join(scratch, "rpmrc"), "w") as f:
            print("include: /usr/lib/rpm/redhat/rpmrc", file=f)
            print("macrofiles: /usr/lib/rpm/macros"
                  ":/usr/lib/rpm/x86_64-linux/macros"
                  ":/usr/lib/rpm/redhat/macros:/etc/rpm/macros.*"
                  ":/etc/rpm/macros:/etc/rpm/x86_64-linux/macros"
                  ":{}/macros".format(scratch),
                  file=f)
        yield scratch


def run(*args, **kw):
    rc = subprocess.call(*args, **kw)
    if rc:
        sys.exit(rc)


def install_requirements(argv=None):
    if argv is None:
        argv = sys.argv
    parser = argparse.ArgumentParser(
        description="Install requirements based on requires.json file.")
    parser.add_argument("-k", "--kind", default="build")
    parser.add_argument("path", default=os.curdir, nargs="?")
    options = parser.parse_args(argv[1:])
    if os.path.isdir(options.path):
        requires = os.path.join(options.path, "requires.json")
    else:
        requires = options.path

    requirements = json.load(open(requires, "r"))
    if options.kind not in requirements:
        print("No", options.kind, "requirements specified.")
    else:
        install_packages(requirements, options.kind)


def main(argv=None):
    if argv is None:
        argv = sys.argv
    options = parse_args(argv[1:])
    vc = get_version_control_plugin(options)
    pkgname = options.name
    if options.parallel:
        pkgname += "-" + options.version
    location = os.path.join(PREFIX, pkgname)

    if options.show:
        print("Name:", pkgname)
        print("Version:", options.version)
        print("Location:", location)
        print("VC URL:", vc.url)
        print("Destination:", options.destination)
        return

    os.umask(0o002)
    try:
        os.mkdir(location)
    except OSError as err:
        if err.errno != errno.EEXIST:
            raise
    os.chdir(location)
    rc, output = vc.export()
    if rc:
        sys.exit(rc)

    requires = os.path.join(location, "requires.json")
    rpm_spec_in = os.path.join(location, "rpm.spec.in")

    if os.path.exists(requires):
        requirements = json.load(open(requires, "r"))
    else:
        requirements = {}

    install_packages(requirements)

    # Now for the show; really building a package here.
    with workspace() as scratch:
        os.chdir(scratch)
        if os.path.exists(rpm_spec_in):
            os.rename(rpm_spec_in, "rpm.spec")
            f = open("rpm.spec", "a")
            print(file=f)
            print(file=f)
        else:
            f = open("rpm.spec", "w")
        dump_requirements(requirements.get("build") or (), "BuildRequires", f)
        dump_requirements(requirements.get("run") or (), "Requires", f)
        spec_content = spec_template().format(
            pkgname=pkgname,
            version=options.version,
            prefix=PREFIX,
            location=location,
            tarname=TARNAME,
            scratch=scratch,
            )
        print(spec_content, file=f)
        f.close()

        run(["rpmbuild", "--rcfile", "rpmrc", "-bb", "rpm.spec"])

        # We're using a glob to avoid having to dig out the name of the
        # local architecture; this also allows for noarch RPMs.
        rpms = glob.glob("*/*.rpm")
        assert len(rpms) == 1, "found more RPMs than expected"

        run(["scp"] + rpms + [options.destination])

    run(["chmod", "-R", "u+w", location])
    shutil.rmtree(location)


def dump_requirements(reqs, label, file):
    for req in reqs:
        version = None
        if isinstance(req, str):
            name = req
        else:
            name = req["name"]
            version = req.get("version")
        
        if version:
            text = "{}: {} = {}".format(label, name, version)
        else:
            text = "{}: {}".format(label, name)
        print(text, file=file)


def install_packages(requirements, kind="build"):
    reqs = collections.OrderedDict()
    for req in requirements.get(kind) or ():
        vspec = ()
        if not isinstance(req, str):
            v = req.get("version")
            req = req["name"]
            if v:
                if "-" in v:
                    vspec = tuple(v.split("-"))
                else:
                    vspec = v,
        reqs[req] = vspec

    if reqs:
        # remove anything that's already installed
        subp = subprocess.Popen(
            ["rpm", "-q"] + list(reqs),
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            shell=False)
        sout, _ = subp.communicate()
        for ln in sout.decode('UTF-8').splitlines():
            if 'is not installed' in ln:
                continue
            name, hversion, hrelease = ln.rsplit("-", 2)
            hspec = hversion, hrelease
            if name not in reqs:
                # Probably a devel package, listed by rpm -q more than
                # once due to multi-arch platform behavior.
                continue
            vspec = reqs[name]
            if hspec[:len(vspec)] == vspec:
                # We have what's specified.
                del reqs[name]
        reqs = ["-".join((name,) + reqs[name]) for name in reqs]

    if reqs:
        run(["sudo", "yum", "clean", "expire-cache"])
        run(["sudo", "yum", "install", "-y"] + reqs)


# Here's how this works:
#
# The software is installed as a single tarball handled by RPM.  When a
# new version is installed, the %post script signals to the %posttrans
# script that it needs to be unpacked by creating a link with the name
# preceeded by a dot.  This name avoids the uninstallation of the old
# package from completely removing the tarball (which a previous version
# would do).  If the version being installed is replacing a version
# created by the version packagegrinder that had that bug, the tarball
# name will be re-created in the %posttrans script.
#
# Once the new version is unpacked, the link with the dot prefix is
# removed so the work isn't done again.

SPEC_TEMPLATE = "rpm.spec.in"

def spec_template():
    dir = os.path.dirname(os.path.abspath(__file__))
    return open(os.path.join(dir, SPEC_TEMPLATE)).read()
