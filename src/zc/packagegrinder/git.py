"""\
Git plugin for package grinder.

"""
__docformat__ = "reStructuredText"

import subprocess
import urllib.parse


class Git:

    pattern = "git@bitbucket.org:zc/{}.git"

    def __init__(self, options):
        url = options.vc_url
        if url:
            self.repository, fragment = urllib.parse.urldefrag(url)
            self.treeish = fragment or options.version
        else:
            self.repository = self.pattern.format(options.project)
            self.treeish = options.version
        self.url = "{}#{}".format(self.repository, self.treeish)

    def export(self):
        git = subprocess.Popen(
            ["git", "archive",
             "--format", "tar", "--remote", self.repository, self.treeish],
            stdout=subprocess.PIPE)
        tar = subprocess.Popen(
            ["tar", "xf", "-"],
            stdin=git.stdout, stdout=subprocess.PIPE)
        git.stdout.close()
        output = tar.communicate()[0]
        return (git.returncode or tar.returncode), output
