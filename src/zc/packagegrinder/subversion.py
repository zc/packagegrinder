"""\
Subversion plugin for package grinder.

"""
__docformat__ = "reStructuredText"

import subprocess


class Subversion:

    pattern = "svn+ssh://svn.zope.com/repos/main/{}/tags/{}-{}-1"

    def __init__(self, options):
        self.url = options.vc_url
        if not self.url:
            self.url = self.pattern.format(
                options.project, options.name, options.version)

    def export(self):
        svn = subprocess.Popen(
            ["svn", "-q", "export", self.url, "--force", "."],
            stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        output = svn.communicate()[0]
        return svn.returncode, output
