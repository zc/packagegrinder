=====================
The Subversion plugin
=====================

The Subversion plugin is used when specified; let's construct a minimal
set of options to see how this looks:

    >>> import zc.packagegrinder
    >>> import zc.packagegrinder.subversion

    >>> options = zc.packagegrinder.parse_args([
    ...     "myproject", "1.2.3", "--subversion"])
    >>> options.vc_plugin
    'subversion'

    >>> plugin = zc.packagegrinder.subversion.Subversion(options)

    >>> plugin.url
    'svn+ssh://svn.zope.com/repos/main/myproject/tags/myproject-1.2.3-1'

If we want to export a tag or branch that isn't named exactly for the
version we're packaging (usually when testing), the ``--vc-url`` option
can be used to control that:

    >>> options = zc.packagegrinder.parse_args([
    ...     "myproject", "1.2.3", "--subversion",
    ...     "--vc-url=http://svn.zope.org/repos/main/zc.iso8601/branches/yo"])
    >>> plugin = zc.packagegrinder.subversion.Subversion(options)

    >>> plugin.url
    'http://svn.zope.org/repos/main/zc.iso8601/branches/yo'


Exporting sources
-----------------

We have a sample repository in the 'repo' directory:

    >>> repository
    'file:///WORKSPACE/repo'

Let's create a git plugin instance that refers to a tag in that
repository:

    >>> options = zc.packagegrinder.parse_args([
    ...     "myproject", "1.2.3", "--vc-url", repository])
    >>> plugin = zc.packagegrinder.subversion.Subversion(options)

    >>> plugin.url
    'file:///WORKSPACE/repo'

The plugin's ``export`` method can be used to extract the content of the
repository into the current directory without including any
meta-information from the version control system:

    >>> import os

    >>> os.mkdir("out")
    >>> os.chdir("out")
    >>> os.getcwd()
    '/WORKSPACE/out'

    >>> os.listdir()
    []

    >>> plugin.export()
    (0, b'')

    >>> sorted(os.listdir())
    ['README.txt', 'src']

    >>> print(open("README.txt").read().rstrip())
    Alternate content.

    >>> print(open("src/module.py").read().rstrip())
    import this

Using ``--vc-url`` we can specify specifc versions from the repository
as well:

    >>> options = zc.packagegrinder.parse_args([
    ...     "myproject", "1.2.3", "--vc-url", repository + "@1"])
    >>> plugin = zc.packagegrinder.subversion.Subversion(options)

    >>> plugin.url
    'file:///WORKSPACE/repo@1'

    >>> os.mkdir("../oot")
    >>> os.chdir("../oot")
    >>> os.getcwd()
    '/WORKSPACE/oot'

    >>> plugin.export()
    (0, b'')

    >>> print(open("README.txt").read().rstrip())
    Sample content.

The return code will be set if there's no matching path in the
repository:

    >>> options = zc.packagegrinder.parse_args([
    ...     "myproject", "1.2.3", "--vc-url", repository + "/missing"])
    >>> plugin = zc.packagegrinder.subversion.Subversion(options)

    >>> plugin.url
    'file:///WORKSPACE/repo/missing'

    >>> plugin.export()
    (1, b"svn: URL 'file:///WORKSPACE/repo/missing' doesn't exist\n")
