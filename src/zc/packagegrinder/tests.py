"""\
Test harness for zc.packagegrinder.

"""
__docformat__ = "reStructuredText"

import doctest
import os
import re
import tempfile
import unittest
import zipfile
import zope.testing.renormalizing
import zope.testing.setupstack


def setup_directory(test):
    tmpdir = tempfile.TemporaryDirectory(prefix="grinder-tests-")
    zope.testing.setupstack.register(test, tmpdir.cleanup)
    zope.testing.setupstack.register(test, os.chdir, os.getcwd())
    os.chdir(tmpdir.name)
    test.globs["repository"] = "file://{}/repo".format(tmpdir.name)

def setup_directory_git(test):
    setup_directory(test)
    unpack("git-repo.zip")

def setup_directory_subversion(test):
    setup_directory(test)
    unpack("subversion-repo.zip")

def unpack(zipname):
    path = os.path.join(os.path.dirname(__file__), zipname)
    zipfile.ZipFile(path).extractall("repo")

checker = zope.testing.renormalizing.RENormalizing([
    (re.compile("/[^/].*grinder-tests-[^/]*/"), "/WORKSPACE/"),
    ])


def test_suite():
    return unittest.TestSuite([
        doctest.DocFileSuite("options.txt"),
        doctest.DocFileSuite("git.txt",
                             checker=checker,
                             setUp=setup_directory_git,
                             tearDown=zope.testing.setupstack.tearDown),
        doctest.DocFileSuite("subversion.txt",
                             checker=checker,
                             setUp=setup_directory_subversion,
                             tearDown=zope.testing.setupstack.tearDown),
        doctest.DocFileSuite("versioncontrol.txt"),
        ])
